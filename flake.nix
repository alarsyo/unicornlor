{
  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "nixos-22.05";
    };

    flake-utils = {
      type = "github";
      owner = "numtide";
      repo = "flake-utils";
      ref = "master";
    };

    rust-overlay = {
      type = "github";
      owner = "oxalica";
      repo = "rust-overlay";
      ref = "master";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
    rust-overlay,
  }: let
    inherit (flake-utils.lib) eachSystem system;
    mySystems = [
      system.aarch64-linux
      system.x86_64-darwin
      system.x86_64-linux
    ];
    eachMySystem = eachSystem mySystems;
  in
    eachMySystem (system: let
      overlays = [(import rust-overlay)];
      pkgs = import nixpkgs {inherit overlays system;};
      my-rust = pkgs.rust-bin.stable.latest.default.override {
        extensions = ["rust-src"];
      };
      inherit (pkgs) lib;
    in {
      devShells = {
        default = pkgs.mkShell {

          nativeBuildInputs = with pkgs; [
            rust-analyzer
            # Clippy, rustfmt, etc...
            my-rust
          ];

          RUST_SRC_PATH = "${my-rust}/lib/rustlib/src/rust/library";
        };
      };
    });
}
